package TikTok;

public class Edge <T>{
    private T source,destination;

    public Edge(T source, T destination){
        this.source = source;
        this.destination = destination;
    }
    public T getSource(){
        return this.source;
    }
    public T getDestination(){
        return this.destination;
    }
}
