package TikTok;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    Map<String, Integer> users;
    private List<List<Edge>> views;
    public Graph() {
        users = new HashMap<>();
        views = new ArrayList<>();
    }

    public void addUser(String user) {
        users.putIfAbsent(user, users.size());
        views.add(new ArrayList<>());
    }

    public void addProfileView(String source, String destination) {
        Edge edge = new Edge(source, destination);
        int sourceIndex = users.get(source);
        for (Map.Entry<String, Integer> entry: users.entrySet()) {
            String key = entry.getKey();
            if (key == destination){
                views.get(sourceIndex).add(edge);
            }
        }
    }

    public void printProfileViews() {
        if(views.size() == 0){
            System.out.println("There are not profile views.");
        }
        for (Map.Entry<String, Integer> entry : users.entrySet()) {
            String vertex = entry.getKey();
            int index = entry.getValue();
            System.out.print("The user "+vertex + " viewed the profile of: ");
            for (Edge edge : views.get(index)) {
                System.out.print(" "+edge.getDestination() +" -");
            }
            System.out.println();
        }
    }
    public String printViews(){
        String aux = "";
        String aux1 = "";
        if(views.size() == 0){
            aux = "There are not profile views.";
        }
        for (int i = 0; i < views.size(); i++) {
            for (int j = 0; j < views.get(i).size(); j++) {
                aux1 = aux;
                aux ="The user "+(String) views.get(i).get(j).getSource() +" viewed the profile of : "+ (String) views.get(i).get(j).getDestination();
            }
            aux = aux1 + " " + aux;

        }
        return aux;
    }
}
