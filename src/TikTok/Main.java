package TikTok;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addUser("Camila");
        graph.addUser("Denis");
        graph.addUser("Axel");
        graph.addUser("Karina");

        graph.addProfileView("Camila","Axel");
        graph.addProfileView("Camila","a");

        graph.printProfileViews();
    }
}
