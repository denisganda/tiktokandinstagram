package Instagram;

import java.util.ArrayList;

public class Graph {

    private final ArrayList<Vertex> usersList;
    private final Utils utils;

    public Graph() {
        this.usersList = new ArrayList<>();
        this.utils = new Utils();
    }

    public ArrayList<Vertex> getUsersList() {
        return usersList;
    }

    public void addUser(Vertex user, Vertex follow) {
        if (!utils.verifyIfExist(user, usersList)) {
            usersList.add(user);
        }
        if (!utils.verifyIfExist(follow, usersList)) {
            usersList.add(follow);
        }
        usersList.get(utils.getPositionUser(user, usersList)).getFollowing().add(follow);
    }
}