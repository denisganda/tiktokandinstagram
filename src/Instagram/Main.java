package Instagram;

public class Main {
    public static void main(String[] args) {
        Printer printer = new Printer();
        Graph graph = new Graph();
        graph.addUser(new Vertex("Axel"), new Vertex("Camila"));
        graph.addUser(new Vertex("Axel"), new Vertex("Denis"));
        graph.addUser(new Vertex("Axel"), new Vertex("Karina"));
        graph.addUser(new Vertex("Denis"), new Vertex("Karina"));
        graph.addUser(new Vertex("Karina"), new Vertex("Camila"));
        graph.addUser(new Vertex("Camila"), new Vertex("Karina"));
        graph.addUser(new Vertex("Denis"), new Vertex("Axel"));

        printer.printAccount(graph);
    }
}
