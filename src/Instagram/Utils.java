package Instagram;

import java.util.ArrayList;

public class Utils {
    public boolean verifyIfExist(Vertex vertex, ArrayList<Vertex> list) {
        boolean result = false;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getUsername().equals(vertex.getUsername())) {
                result = true;
                break;
            }
        }

        return result;
    }

    public int getPositionUser(Vertex user, ArrayList<Vertex> usersList) {
        int position = 0;
        for (int i = 0; i < usersList.size(); i++) {
            if (user.getUsername().equals(usersList.get(i).getUsername())) {
                position = i;
                break;
            }
        }
        return position;
    }
}
