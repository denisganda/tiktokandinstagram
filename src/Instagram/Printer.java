package Instagram;

public class Printer {
    public void printAccount(Graph graph) {
        for (int i = 0; i < graph.getUsersList().size(); i++) {
            System.out.print(graph.getUsersList().get(i).getUsername() + "\t -> |");
            for (int j = 0; j < graph.getUsersList().get(i).getFollowing().size(); j++) {
                System.out.print(graph.getUsersList().get(i).getFollowing().get(j).getUsername() + " -> ");
            }
            System.out.print(" x \n");
        }
    }
}
