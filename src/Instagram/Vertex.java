package Instagram;

import java.util.ArrayList;

public class Vertex {
    private String username;
    private final ArrayList<Vertex> following;
    private final Utils utils;

    public Vertex(String username) {
        this.username = username;
        this.following = new ArrayList<>();
        this.utils = new Utils();
    }

    public ArrayList<Vertex> getFollowing() {
        return following;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void addFollow(Vertex vertex) {
        if (!utils.verifyIfExist(vertex, following)) {
            following.add(vertex);
        }
    }
}
