package TikTokTest;

import TikTok.Graph;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TikTokTest {
    @Test
    public void TikToKTest(){
        Graph graph = new Graph();
        graph.addUser("Camila");
        graph.addUser("Paula");
        graph.addProfileView("Camila","Paula");
        assertEquals("  The user Camila viewed the profile of : Paula",graph.printViews());

    }

    @Test
    public void TikToKTest2(){
        Graph graph = new Graph();
        graph.addUser("Camila");
        graph.addUser("Paula");
        graph.addUser("Carlos");
        graph.addUser("Maria");

        graph.addProfileView("Camila","Paula");
        graph.addProfileView("Maria","Carlos");
        assertEquals("   The user Camila viewed the profile of : Paula The user Maria viewed the profile of : Carlos",graph.printViews());

    }

    @Test
    public void TikToKTest3(){
        Graph graph = new Graph();
        graph.addUser("Camila");
        graph.addUser("Paula");
        graph.addUser("Carlos");
        graph.addUser("Maria");

        graph.addProfileView("Camila","Pepe");
        assertEquals("    ",graph.printViews());
    }

    @Test
    public void TikToKTest4(){
        Graph graph = new Graph();
        assertEquals("There are not profile views.",graph.printViews());
    }
}
