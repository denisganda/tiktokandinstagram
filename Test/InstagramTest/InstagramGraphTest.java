package InstagramTest;

import Instagram.Graph;
import Instagram.Vertex;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class InstagramGraphTest {
    @Test
    public void testAddUser() {
        Graph graph = new Graph();
        graph.addUser(new Vertex("Axel"), new Vertex("Camila"));
        graph.addUser(new Vertex("Hector"), new Vertex("Camila"));

        // Expected
        int expected = 3;

        // Actual
        int actual = graph.getUsersList().size();

        assertEquals(expected, actual);
    }

    @Test
    public void testVerifyFollowersFromUser() {
        Graph graph = new Graph();
        graph.addUser(new Vertex("Mateo"), new Vertex("Camila"));
        graph.addUser(new Vertex("Mateo"), new Vertex("Arturo"));
        graph.addUser(new Vertex("Mateo"), new Vertex("Amilcar"));
        graph.addUser(new Vertex("Alan"), new Vertex("Cesar"));
        graph.addUser(new Vertex("Alan"), new Vertex("Veronica"));

        // Actual Mateo
        String actualFollowersMateo = getFollowers(0, graph);
        // Expected Mateo
        String expectedFollowersMateo = "Camila Arturo Amilcar ";
        assertEquals(expectedFollowersMateo, actualFollowersMateo);

        // Actual Alan
        String actualFollowersAlan = getFollowers(4, graph);
        // Expected Alan
        String expectedFollowersAlan = "Cesar Veronica ";
        assertEquals(expectedFollowersAlan, actualFollowersAlan);
    }

     @Test
     public void testGraphOrUserListEmpty() {
         Graph graph = new Graph();

         // Actual
         int sizeUserList = graph.getUsersList().size();

         // Expected
         int expected = 0;
         assertEquals(expected, sizeUserList);
     }

     @Test
     public void testVerifyAmountFollowersFromUser() {
         Graph graph = new Graph();
         graph.addUser(new Vertex("Mateo"), new Vertex("Camila"));
         graph.addUser(new Vertex("Mateo"), new Vertex("Arturo"));
         graph.addUser(new Vertex("Mateo"), new Vertex("Amilcar"));
         graph.addUser(new Vertex("Alan"), new Vertex("Cesar"));
         graph.addUser(new Vertex("Alan"), new Vertex("Veronica"));
         graph.addUser(new Vertex("Ignacio"), new Vertex("Michelle"));

         // Actual Mateo
         int actualMateo = 3;
         // Expected Mateo
         int expectedAmountFollowersMateo = graph.getUsersList().get(0).getFollowing().size();
         assertEquals(expectedAmountFollowersMateo, actualMateo);


         // Actual Alan
         int actualAlan = 2;
         // Expected Alan
         int expectedAmountFollowersAlan = graph.getUsersList().get(4).getFollowing().size();
         assertEquals(expectedAmountFollowersAlan, actualAlan);


         // Actual Ignacio
         int actualIgnacio = 1;
         // Expected Ignacio
         int expectedAmountFollowersIgnacio = graph.getUsersList().get(7).getFollowing().size();
         assertEquals(expectedAmountFollowersIgnacio, actualIgnacio);
     }

    private String getFollowers(int position, Graph graph) {
        String followers = "";
        for (int j = 0; j < graph.getUsersList().get(position).getFollowing().size(); j++) {
            followers += graph.getUsersList().get(position).getFollowing().get(j).getUsername() + " ";
        }
        return followers;
    }
}
